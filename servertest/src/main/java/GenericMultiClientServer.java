/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.ServerSocket;


/**
 *
 * @author claudiu
 */
public class GenericMultiClientServer {   
    public static void main(String[] args) throws IOException 
    {

        int portNumber = 8080;
        
        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
             
            System.out.println("Asteapta conexiune client pe portul " +
            serverSocket.getLocalPort() + "...");
            
            while (true) {
                new ServerThread(serverSocket.accept()).start();
            }
            
        } catch (IOException e) {
            System.err.println("Conexiune esuata pe portul " + portNumber);
            System.exit(-1);
        }
    }
    
}


